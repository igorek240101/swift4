enum Speed : String //Создайте по 2 enum с разным типом RawValue
{
    case Quick = "Quick"
    case Slow = "Slow"
}

enum Mask : Int //Создайте по 2 enum с разным типом RawValue
{
    case FirstBit = 1
    case SecondBit = 2
    case LastBit = 4
}

struct Employee // Создайте несколько enum для заполнения полей стркутуры - анкета сотрудника: enum пол, enum категория возраста, enum стаж
{
    let sex : Sex;
    var age : Age;
    var epirience : Expirience;
    enum Sex
    {
        case Male
        case Female
    }
    enum Age
    {
        case Yong
        case Midle
        case Old
    }
    enum Expirience
    {
        case Junior
        case Midle
        case Senior
    }
}

enum Color : String // Создать enum со всеми цветами радуги
{
    case Red = "\u{001B}[0;31m" // Это Escepe последовательности обозначающие цвета
    case Orange = "\u{001B}[0;33m"
    case Yellow = "\u{001B}[0;93m"
    case Green = "\u{001B}[0;32m"
    case Blue = "\u{001B}[0;36m"
    case Indigo = "\u{001B}[0;34m"
    case Violet = "\u{001B}[0;35m"
}

func enumArrays() // Создать функцию, которая содержит массив разных case'ов enum и выводит содержимое в консоль.
{
    let enums = [ Speed.Slow, Speed.Quick];
    print(enums) // Здесь выведется но немного с мусором
    for i in enums
    {
        print(i.rawValue); // А здесь чисто выведеться 
    }
}

enumArrays();

enum Score : String // Оценки немного на Американский манер
{
    case A = "A"
    case B = "B"
    case C = "C"
    case F = "F"
}

func score(score : Score) // Создать функцию, которая выставляет оценки ученикам в школе enum Score: String и выводит числовое значение оценки
{
    switch score 
    {
        case .A : print(5);
        case .B : print(4);
        case .C : print(3);
        case .F : print(2);
    }
}

score(score : Score(rawValue : "A")!); // Инициализация через rawValue
score(score : Score(rawValue : "F")!);
score(score : Score(rawValue : "C")!);


enum Cars // Машина в гараже у каждой свои дополнительные свойства
{
    case Impala (year : Int)
    case BMV (color : Color)
    case Tractor (speed : Speed, wheelsCount : Int)
}

func garage(car : Cars)
{
    switch car
    {
        // Для импалы по году покупки определим дату разработки
        case .Impala(let release) where release <= 1958 : print("Импала 1957 года");
        case .Impala(let release) where release <= 1960 : print("Импала 1959 года");
        case .Impala(let release) where release <= 1964 : print("Импала 1960 года");
        case .Impala(let release) where release <= 1970 : print("Импала 1965 года");
        case .Impala(let release) where release <= 1976 : print("Импала 1971 года");
        case .Impala(let release) where release <= 1994 : print("Импала 1977 года");
        case .Impala(let release) where release <= 2000 : print("Импала 1994 года");
        case .Impala(let release) where release <= 2005 : print("Импала 2000 года");
        case .Impala(let release) where release <= 2011 : print("Импала 2006 года");
        case .Impala(let release) where release <= 2012 : print("Импала 2012 года");
        case .Impala : print("Импала 2013 года");
        // Текст "БМВ" покрасим в тот цвет, какого цвета машина
        case .BMV(let color) : print("\(color.rawValue)БМВ\u{001B}[0;0m");
        // Трактор с двойным switch-ом
        case .Tractor(let speed, let wheels) : do
        {
            switch speed
            {
                case .Quick : print("Быстрый трактор c \(wheels) колесами")
                case .Slow : print("Медленный трактор c \(wheels) колесами")
            }
        }
    }
}

garage(car : Cars.BMV(color : Color.Violet));